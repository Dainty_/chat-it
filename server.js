const express = require("express");
const path = require("path");
const http = require("http");
const moment = require("moment-timezone");
const socketio = require("socket.io");

const app = express();
const server = http.createServer(app);
const io = socketio(server);
let users = [];
app.use(express.static(path.join(__dirname, "public")));

const PORT = 3000 || process.env.PORT;

//Runs when client connects
io.on("connection", (socket) => {
  socket.on("user", (name) => {
    users.push(name);
    console.log(users);
    io.emit("activeUser", users);
    //Emit to the new connection alone
    socket.emit("message", {
      msg: `Welcome to Chat-It ${name}`,
      bot: true,
      time: moment().tz("Asia/Kolkata").format("MMM Do YYYY, hh:mm a"),
      notify: false,
    });
    //Broadcast to users other than new user when a user connects
    socket.broadcast.emit("message", {
      msg: `${name} has joined the chat !`,
      bot: true,
      time: moment().tz("Asia/Kolkata").format("MMM Do YYYY, hh:mm a"),
      notify: true,
    });
    socket.on("disconnect", () => {
      //Removes the user from active list
      let index = users.indexOf(name);
      users.splice(index, 1);
      console.log(users);

      io.emit("activeUser", users);

      io.emit("message", {
        msg: `${name} has left the chat`,
        bot: true,
        time: moment().tz("Asia/Kolkata").format("MMM Do YYYY, hh:mm a"),
        notify: false,
      }); //To all clients
    });
  });

  socket.on("typing", (name) => {
    socket.broadcast.emit("type", name);
  });

  socket.on("clearText", () => {
    socket.broadcast.emit("clearText");
  });

  socket.on("userText", (data) => {
    socket.broadcast.emit("message", {
      msg: data.msg,
      bot: false,
      time: moment().tz("Asia/Kolkata").format("MMM Do YYYY, hh:mm a"),
      user: data.name,
      right: false,
    });

    socket.emit("message", {
      msg: data.msg,
      bot: false,
      time: moment().tz("Asia/Kolkata").format("MMM Do YYYY, hh:mm a"),
      user: data.name,
      right: true,
    });
  });
});
server.listen(PORT, () => console.log(`Listening on port ${PORT}`));
