const chatForm = document.getElementById("msg");
const allMessages = document.querySelector(".allMessages");
const textArea = document.querySelector(".status");
const arrow = document.getElementsByClassName("arrow");
let activeUsers = document.querySelector(".topScroll");
const left = document.getElementById("left");
const right = document.getElementById("right");
const active = document.getElementById("Active");

const socket = io();
let params = new URLSearchParams(document.location.search.substring(1));
let userName = params.get("name");
userName == "" || userName === undefined || userName === null
  ? (userName = "USER")
  : (userName = userName);
console.log(userName);
let allUsers = [];

let updateUser = (users) => {
  activeUsers.innerHTML = " ";
  users.forEach((val) => {
    activeUsers.innerHTML += `<div class="users">
    <img class="userPic" src="./Images/user.png" alt='userpic'>
    <div class="scrollName"><b>${val}</b></div>
</div >`;
  });
  if (activeUsers.clientWidth < activeUsers.scrollWidth) {
    right.style.display = "inline-block";
    left.style.display = "inline-block";
  } else {
    right.style.display = "none";
    left.style.display = "none";
  }
};

socket.emit("user", userName);

socket.on("message", (msg) => {
  console.log(msg);
  msg.bot ? logAlert(msg) : logMessage(msg);
  //To scroll down
  allMessages.scrollTop = allMessages.scrollHeight;
});

socket.on("type", (user) => {
  textArea.innerHTML = `${user} is typing ....`;
});

socket.on("clearText", () => {
  textArea.innerHTML = " ";
});

socket.on("activeUser", (users) => {
  updateUser(users);
  allUsers = users;
});

left.addEventListener("click", () => {
  activeUsers.scrollLeft -= 100;
});
right.addEventListener("click", () => {
  activeUsers.scrollLeft += 100;
});

window.addEventListener("resize", () => {
  updateUser(allUsers);
});

chatForm.addEventListener("keydown", () => {
  socket.emit("typing", userName);
});

chatForm.addEventListener("submit", (event) => {
  event.preventDefault();

  let text = event.target.elements.userText.value;
  if (text != "") socket.emit("userText", { msg: text, name: userName });
  socket.emit("clearText");
  //Clear text
  event.target.elements.userText.value = "";
});

function logMessage(message) {
  const div = document.createElement("div");
  div.classList.add("texts");
  message.right
    ? ((div.style.marginLeft = "25%"), (div.style.backgroundColor = "#ffeab4"))
    : ((div.style.marginRight = "25%"),
      (div.style.backgroundColor = "#cdf1e4"));

  div.innerHTML = `<span class='msghead'><b>${
    message.right ? "You" : message.user
  }</b> <span><span class="msgTime"> ${message.time.split(",")[0]},</span>${
    message.time.split(",")[1]
  }</span></span>
  <div>${message.msg}</div>`;
  document.querySelector(".allMessages").appendChild(div);
}

function logAlert(message) {
  const div = document.createElement("div");
  div.innerHTML = `<div class='alert'><b>${message.msg}</b><span class='alertTime'> @ ${message.time}</span></div>`;
  document.querySelector(".allMessages").appendChild(div);
  if (message.notify && Notification.permission === "granted") {
    const notification = new Notification("Welcome new user !", {
      body: `${message.msg}`,
    });
  }
}
if (Notification.permission !== "denied") {
  Notification.requestPermission().then((permission) => {
    console.log(permission);
  });
}
